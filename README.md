## Samba-Server

Readme

#### Volume:

- /mnt

#### Port:

- 137/udp
- 138/udp
- 139/tcp
- 445/tcp

#### Custom usage:

    docker run \
        -d \
        --name samba-server \
        -p 137:137/udp \
        -p 138:138/udp \
        -p 139:139/tcp \
        -p 445:445/tcp \
        -e PASSWORD=yourpassword \
        -v /your/path:/mnt \
        walchinsky/samba-server

#### Compose example:
```
version: "3.7"
services:
  samba-server:
    image: walchinsky/samba-server
    container_name: samba-server
    ports:
      - "137:137/udp"
      - "138:138/udp"
      - "139:139/tcp"
      - "445:445/tcp"
    volumes:
      - ./smb.conf:/etc/samba/smb.conf:ro
      - /your/path:/mnt
    environment:
      - PASSWORD=yourpassword
    restart: unless-stopped
```
